<!--
SPDX-FileCopyrightText: 2022 BC Lib Developers

SPDX-License-Identifier: CC0-1.0
-->

# Contributing

This (WIP) guide details important aspects for those contributing to the project. 
In particular, it discusses code and issues.

## Contributing with Code

Please check the licenses and code of conduct first.

### Code style

We use rust-analyzer (https://rust-analyzer.github.io/) and clippy
(https://github.com/rust-lang/rust-clippy). Otherwise, please aim to be as
idiomatic as possible.

### Semantic Versioning

We will use proper Semantic Versioning (https://semver.org) when we settle on
our first public API.

### Tests

1. Existing tests: your contribution must pass all the unit tests provided
   to be accepted.
2. If necessary change the tests accordingly to your change.
3. New code must have a decent coverage by unit tests.

### Branches and Forks

If you are a member of the "core group", you should have maintainer access, and
thus can contribute by creating branches. Remember to not rebase on the *main*
branch. Also, I suggest always merging the *main* branch locally unto your
feature branch before pushing.

Otherwise, you should do a pull request (remember to add proper license
headers and ensure that tests pass).

### Code Review

While we still have not fixed a policy for code review, we expect to able to
respond to issues in at most a week. After a pull request, the project
managers will assess and decide whether a contribution will be accepted or
not, giving the adequate feedback to the contributor.

### Licenses and Headers

We use REUSE (https://reuse.software/) to comply with licensing recommendations.
When in doubt, remember the following example:

``pipx run reuse addheader --copyright="BC Lib Developers" --license="Apache-2.0" Cargo.toml src/lib.rs``

## Commit Style

See: https://chris.beams.io/posts/git-commit/

### Contributing with Issues

## Bug Reports

When filing a bug report, make sure to answer these five questions:

1. What version you are using (or commit / branch)?
2. What operating system and processor architecture are you using?
3. What did you do?
4. What was the expected outcome?
5. What was the actual outcome?

## Suggesting new features and enhancements

Currently, we will are still implementing the bare bones of the project,
therefore these requests will not be attended at this time.
