// SPDX-FileCopyrightText: 2022 BC Lib Developers
//
// SPDX-License-Identifier: Apache-2.0

pub trait HSDeque<T> {
    fn add(&mut self, element: T);
    fn take(&mut self) -> Option<T>;
    fn empty(&self) -> bool;
}

impl<T> HSDeque<T> for Vec<T> {
    fn add(self: &mut std::vec::Vec<T>, element: T) {
        self.push(element);
    }

    fn take(self: &mut std::vec::Vec<T>) -> Option<T> {
        self.pop()
    }

    fn empty(&self) -> bool {
        self.is_empty()
    }
}
