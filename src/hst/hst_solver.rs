// SPDX-FileCopyrightText: 2022 BC Lib Developers
//
// SPDX-License-Identifier: Apache-2.0

use std::{collections::HashSet, marker::PhantomData};

use crate::formula::Formula;

pub type HSTRepr<T> = im::HashSet<T>;

pub struct HSTResult<'a, T>
where
    T: Formula,
{
    nodes: HashSet<im::HashSet<T>>,
    paths: HashSet<im::HashSet<T>>,
    _phantom_data: std::marker::PhantomData<&'a ()>,
}

impl<T> Clone for HSTResult<'_, T>
where
    T: Formula,
{
    fn clone(&self) -> Self {
        Self {
            nodes: self.nodes.clone(),
            paths: self.paths.clone(),
            _phantom_data: self._phantom_data,
        }
    }
}

pub(crate) fn overlaps<T>(hst_path: &HSTRepr<T>, element: &HSTRepr<T>) -> bool
where
    T: Formula,
{
    for x in hst_path {
        if element.contains(x) {
            return true;
        }
    }
    false
}

impl<'a, T> HSTResult<'a, T>
where
    T: Formula,
{
    pub fn new() -> Self {
        Self {
            nodes: HashSet::new(),
            paths: HashSet::new(),
            _phantom_data: PhantomData,
        }
    }

    /// Get a reference to the hstresult's nodes.
    #[must_use]
    pub fn nodes(&self) -> &HashSet<im::HashSet<T>> {
        &self.nodes
    }

    /// Get a mutable reference to the hstresult's nodes.
    #[must_use]
    pub fn nodes_mut(&mut self) -> &mut HashSet<im::HashSet<T>> {
        &mut self.nodes
    }

    /// Get a reference to the hstresult's paths.
    #[must_use]
    pub fn paths(&self) -> &HashSet<im::HashSet<T>> {
        &self.paths
    }

    /// Get a mutable reference to the hstresult's paths.
    #[must_use]
    pub fn paths_mut(&mut self) -> &mut HashSet<im::HashSet<T>> {
        &mut self.paths
    }
}

impl<'a, T> Default for HSTResult<'a, T>
where
    T: Formula,
{
    fn default() -> Self {
        Self::new()
    }
}

/// HST general method.
pub trait HSTCalculator<T>
where
    T: Formula,
{
    fn hitting_set(&mut self) -> HSTResult<T>;
    fn reusable(&self, hst_path: &HSTRepr<T>) -> Option<&HSTRepr<T>>;
    fn should_not_terminate(&self, hst_path: &HSTRepr<T>) -> bool;
    fn get_node(&self, hst_path: &HSTRepr<T>) -> Option<HSTRepr<T>>;
    fn close_path(&mut self, hst_path: HSTRepr<T>);
    fn early_return(&self) -> bool {
        false
    }
    fn successors<'a>(&self, hst_path: &'a HSTRepr<T>, node: &'a HSTRepr<T>) -> Vec<HSTRepr<T>>;
    fn hst_step(&mut self) -> HSTResult<T> {
        let mut queue = vec![im::HashSet::new()];
        let mut result = HSTResult {
            nodes: HashSet::new(),
            paths: HashSet::new(),
            _phantom_data: PhantomData,
        };

        while let Some(hst_path) = queue.pop() {
            if self.early_return() {
                break;
            }
            let mut created_node = false;
            let reusable = self.reusable(&hst_path);
            let mut opt_node = None;

            if reusable.is_none() {
                created_node = true;
                opt_node = self.get_node(&hst_path);
            }
            if let Some(node) = opt_node {
                if created_node {
                    result.nodes.insert(node.clone());
                }

                let successors = self.successors(&hst_path, &node);

                for x in successors {
                    if self.should_not_terminate(&x) {
                        queue.push(x)
                    }
                }
            } else {
                //result.paths.insert(hst_path);
                self.close_path(hst_path);
            }
        }
        result
    }
}
