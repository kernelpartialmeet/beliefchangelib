// SPDX-FileCopyrightText: 2022 BC Lib Developers
//
// SPDX-License-Identifier: Apache-2.0

//! # MinImp Module
//! This module contains the functions and structures needed for computing
//! MinImp(s) (kernel, justifications, MIPS, MUS)

pub mod classical_mie;
pub mod classical_mis;
pub mod divider_conquer_mis;
pub mod mic;
pub mod trivial_mie;
