// SPDX-FileCopyrightText: 2022 BC Lib Developers
//
// SPDX-License-Identifier: Apache-2.0

use crate::{
    formula::Formula,
    hst::hst_solver::{overlaps, HSTCalculator, HSTRepr, HSTResult},
    BaseRepr,
};
use std::collections::HashSet;

/// A MinImp calculator for sets of things with type T (which must implement Formula).
///
/// A MinImp is a minimal subset of a base |
pub trait MICalculator<T>
where
    T: Formula,
{
    /// Returns Some(MinImp), if any and None otherwise.
    fn minimp(&self, base: &BaseRepr<T>) -> Option<BaseRepr<T>>;
}

/// Enlargement strategy for MinImp computation.
pub trait MIEnlarger<T>
where
    T: Formula,
{
    /// Returns a subset of the base that passes the checker, if any.
    /// Yields None otherwise.
    fn enlarge(&self, base: &BaseRepr<T>) -> Option<BaseRepr<T>>;
}

/// Shrinking strategy for MI supersets (e.g. provided by a MIEnlarger).
pub trait MIShrinker<T>
where
    T: Formula,
{
    /// Returns an Option object with a minimal subset of the input that satisfies the checker or
    /// None if no such subset exists.
    fn shrink(&self, enlarged: &mut BaseRepr<T>) -> Option<BaseRepr<T>>;
}


/// Structure for black-box computation of a MinImp using an enlarge/shrink strategy.
pub struct BlackBoxMIC<T> {
    enlarger: Box<dyn MIEnlarger<T>>,
    shrinker: Box<dyn MIShrinker<T>>,
}

impl<T> BlackBoxMIC<T> {
    /// Creates a new black-box MinImp calculator with the given strategies
    pub fn new(enlarger: Box<dyn MIEnlarger<T>>, shrinker: Box<dyn MIShrinker<T>>) -> Self {
        Self { enlarger, shrinker }
    }
}

impl<T> MICalculator<T> for BlackBoxMIC<T>
where
    T: Formula,
{
    fn minimp<'a>(&self, base: &BaseRepr<T>) -> Option<BaseRepr<T>> {
        if let Some(mut expanded) = self.enlarger.enlarge(base) {
            if let Some(shrunken) = self.shrinker.shrink(&mut expanded) {
                return Some(shrunken);
            }
            return None;
        }
        None
    }
}

/// Computes a set of MinImps.
pub trait MISCalculator<T>
where
    T: Formula,
{
    /// Returns the set of MinImps.
    fn minimps(&mut self) -> HashSet<HSTRepr<T>>;
}

pub struct HSTMISCalculator<'a, T>
where
    T: Formula,
{
    base: &'a BaseRepr<T>,
    mic: Box<dyn MICalculator<T>>,
    result: HSTResult<'a, T>,
}

impl<'a, T> HSTMISCalculator<'a, T>
where
    T: Formula,
{
    pub fn new(base: &'a BaseRepr<T>, mic: Box<dyn MICalculator<T>>) -> Self {
        Self {
            base,
            mic,
            result: HSTResult::new(),
        }
    }

    /// Get a mutable reference to the hstmiscalculator's result.
    pub fn result_mut(&mut self) -> &mut HSTResult<'a, T> {
        &mut self.result
    }
}

impl<'a, T> MISCalculator<T> for HSTMISCalculator<'a, T>
where
    T: Formula,
{
    fn minimps(self: &mut HSTMISCalculator<'a, T>) -> HashSet<HSTRepr<T>> {
        self.hitting_set().nodes().clone()
    }
}

impl<'a, T> HSTCalculator<T> for HSTMISCalculator<'a, T>
where
    T: Formula,
{
    fn hitting_set(self: &mut HSTMISCalculator<'a, T>) -> HSTResult<T> {
        self.hst_step();
        self.result.clone()
    }

    fn reusable(&self, hst_path: &HSTRepr<T>) -> Option<&HSTRepr<T>> {
        let x = self.result.nodes();
        for element in x {
            if !overlaps(hst_path, element) {
                return Some(element);
            }
        }
        None
    }

    fn should_not_terminate(&self, hst_path: &HSTRepr<T>) -> bool {
        !self.result.paths().iter().any(|x| x.is_subset(hst_path))
    }

    fn get_node(&self, hst_path: &HSTRepr<T>) -> Option<HSTRepr<T>> {
        let mut im_base: HSTRepr<T> = im::HashSet::new();

        for x in self.base {
            im_base.insert(x.clone());
        }

        let tmp_base = im_base.relative_complement(hst_path.clone());
        let test_set = HashSet::from_iter(tmp_base.into_iter());

        if let Some(kernel) = self.mic.minimp(&test_set) {
            return Some(im::HashSet::from(kernel));
        }
        None
    }

    fn close_path(self: &mut HSTMISCalculator<'a, T>, hst_path: HSTRepr<T>) {
        self.result_mut().paths_mut().insert(hst_path);
    }

    fn successors(&self, hst_path: &HSTRepr<T>, node: &HSTRepr<T>) -> Vec<HSTRepr<T>> {
        let mut result = Vec::with_capacity(node.len());
        for element in node {
            result.push(im::HashSet::unit(element.clone()).union(hst_path.clone()));
        }
        result
    }
}

#[cfg(test)]
mod tests {
    use std::{collections::HashSet, rc::Rc};

    use crate::{
        checker::{AtLeastTwoChecker, Checker},
        mi::{classical_mis::ClassicalMIS, mic::MISCalculator, trivial_mie::TrivialMIE},
        BaseRepr,
    };

    use super::{BlackBoxMIC, HSTMISCalculator};

    #[test]
    fn simple_mic() {
        let str_vec = ["hello", "rust", "world"];
        let mut string_vec = Vec::new();
        let mut base: BaseRepr<String> = HashSet::with_capacity(3);

        for some_str in str_vec {
            string_vec.push(String::from(some_str));
        }

        for x in string_vec {
            base.insert(x);
        }

        let checker = AtLeastTwoChecker::new();
        let checker_rc: Rc<dyn Checker<String>> = Rc::new(checker);
        let mie = TrivialMIE::new(Rc::clone(&checker_rc));
        let mis = ClassicalMIS::new(Rc::clone(&checker_rc));

        let mic = BlackBoxMIC::new(Box::new(mie), Box::new(mis));
        let mut misc = HSTMISCalculator::new(&base, Box::new(mic));

        assert_eq!(misc.minimps().len(), 3);
    }
}
