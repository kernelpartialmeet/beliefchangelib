// SPDX-FileCopyrightText: 2022 BC Lib Developers
//
// SPDX-License-Identifier: Apache-2.0

use super::mic::BlackBoxMIC;

pub struct HSTMIC {
}

impl BlackBoxMIC for HSTMIC {
}
