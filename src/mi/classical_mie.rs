// SPDX-FileCopyrightText: 2022 BC Lib Developers
//
// SPDX-License-Identifier: Apache-2.0

use std::rc::Rc;

use super::mic::MIEnlarger;
use crate::{checker::Checker, formula::Formula, BaseRepr};

pub struct ClassicalMIE<T>
where
    T: Formula,
{
    checker: Rc<dyn Checker<T>>,
}

impl<T> ClassicalMIE<T>
where
    T: Formula,
{
    fn new(checker: Rc<dyn Checker<T>>) -> Self {
        Self { checker }
    }
}

impl<T> MIEnlarger<T> for ClassicalMIE<T>
where
    T: Formula,
{
    fn enlarge(&self, base: &BaseRepr<T>) -> Option<BaseRepr<T>> {
        let mut enlarged = BaseRepr::with_capacity(base.len());

        for form in base.iter() {
            enlarged.insert(form.clone());
            if self.checker.check(&enlarged) {
                enlarged.remove(form);
            }
        }

        Some(enlarged)
    }
}

mod tests {
    
}
