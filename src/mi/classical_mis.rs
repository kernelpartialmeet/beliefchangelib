// SPDX-FileCopyrightText: 2022 BC Lib Developers
//
// SPDX-License-Identifier: Apache-2.0

use std::option::Option;
use std::rc::Rc;

use crate::checker::Checker;
use crate::formula::Formula;
use crate::BaseRepr;

use super::mic::MIShrinker;

pub struct ClassicalMIS<T>
where
    T: Formula,
{
    checker: Rc<dyn Checker<T>>,
}

impl<T> ClassicalMIS<T>
where
    T: Formula,
{
    pub fn new(checker: Rc<dyn Checker<T>>) -> Self {
        Self { checker }
    }
}

pub(crate) fn extra_s_print<T>(base: &BaseRepr<T>)
where
    T: Formula,
{
    for elt in base {
        println!("{:?} ({:p})", elt, elt);
    }
}

fn print_type_of<T>(_: &T) {
    println!("{}", std::any::type_name::<T>())
}

impl<T> MIShrinker<T> for ClassicalMIS<T>
where
    T: Formula,
{
    fn shrink<'a>(&self, enlarged: &mut BaseRepr<T>) -> Option<BaseRepr<T>> {
        println!("Shrinking");
        let copy = enlarged.clone();
        let mut candidate = BaseRepr::with_capacity(copy.len());
        candidate.extend(copy.clone());
        let seq = copy.iter();
        let mut last_removed = None;
        for elt in seq {
            let x = elt;
            print_type_of(x);
            println!("Candidate");
            extra_s_print(&candidate);
            if self.checker.check(&candidate) {
                last_removed = candidate.take(elt);
                print_type_of(&last_removed);
            } else {
                for y in &candidate {
                    println!("{:?} vs {:?} = {:?}", &y, &x, x == y);
                }
                println!("Candidate In");
                extra_s_print(&candidate);
                if let Some(value) = last_removed {
                    candidate.insert(value);
                }
                println!("Candidate Out");
                extra_s_print(&candidate);
                return Some(candidate);
            }
        }
        if self.checker.check(&candidate) {
            None
        } else {
            Some(candidate)
        }
    }
}
