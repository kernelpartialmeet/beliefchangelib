// SPDX-FileCopyrightText: 2022 BC Lib Developers
//
// SPDX-License-Identifier: Apache-2.0

use std::{collections::HashSet, rc::Rc};

use crate::{checker::Checker, formula::Formula};

use super::mic::MIShrinker;
use crate::BaseRepr;

pub struct DivideAndConquerMIS<T>
where
    T: Formula,
{
    checker: Rc<dyn Checker<T>>,
}

impl<T> DivideAndConquerMIS<T>
where
    T: Formula,
{
    pub fn new(checker: Rc<dyn Checker<T>>) -> Self {
        DivideAndConquerMIS { checker }
    }

    fn divide_conquer<'a>(
        &self,
        support: &BaseRepr<T>,
        enlarged: &mut BaseRepr<T>,
    ) -> Option<BaseRepr<T>> {
        //Base case
        if enlarged.len() <= 1 {
            return Some(enlarged.clone());
        }

        let middle = enlarged.len() / 2;

        let mut first_half = BaseRepr::with_capacity(middle);
        let mut second_half = BaseRepr::with_capacity(enlarged.len() - middle);

        let mut count = 1;
        let mut iter_enlarged = enlarged.iter();

        while count <= middle {
            let form_op = iter_enlarged.next();
            match form_op {
                Some(form) => {
                    first_half.insert(form.clone());
                }
                None => {
                    return None;
                }
            }

            count += 1;
        }
        while count <= enlarged.len() {
            let form_op = iter_enlarged.next();
            match form_op {
                Some(form) => {
                    second_half.insert(form.clone());
                }
                None => {
                    return None;
                }
            }
            count += 1;
        }

        let first_union: HashSet<T> = support.union(&first_half).cloned().collect();

        if self.checker.check(&first_union) {
            return self.divide_conquer(support, &mut first_half);
        }

        let second_union: HashSet<T> = support.union(&first_half).cloned().collect();

        if self.checker.check(&second_union) {
            return self.divide_conquer(support, &mut second_half);
        }

        let union_sup_2nd_half: HashSet<T> = support.union(&second_half).cloned().collect();
        let union_sup_1st_half: HashSet<T> = support.union(&first_half).cloned().collect();

        let recombined_1st = self.divide_conquer(&union_sup_2nd_half, &mut first_half);
        let recombined_2nd = self.divide_conquer(&union_sup_1st_half, &mut second_half);

        if let Some(comb_1) = recombined_1st {
            if let Some(comb_2) = recombined_2nd {
                let res: HashSet<T> = comb_1.union(&comb_2).cloned().collect();
                return Some(res);
            }
        }

        None
    }
}

impl<T> MIShrinker<T> for DivideAndConquerMIS<T>
where
    T: Formula,
{
    fn shrink(&self, enlarged: &mut BaseRepr<T>) -> Option<BaseRepr<T>> {
        let support = HashSet::new();
        self.divide_conquer(&support, enlarged)
    }
}
