// SPDX-FileCopyrightText: 2022 BC Lib Developers
//
// SPDX-License-Identifier: Apache-2.0

use std::collections::HashSet;
use std::rc::Rc;

use crate::checker::Checker;
use crate::formula::Formula;
use crate::BaseRepr;

use super::mic::MIEnlarger;

/// Trivial strategy for enlarging sets until a MinImp is obtained.
pub struct TrivialMIE<T>
where
    T: Formula,
{
    checker: Rc<dyn Checker<T>>,
}

impl<T> TrivialMIE<T>
where
    T: Formula,
{
    pub fn new(checker: Rc<dyn Checker<T>>) -> Self {
        Self { checker }
    }
}

impl<T> MIEnlarger<T> for TrivialMIE<T>
where
    T: Formula,
{
    fn enlarge(&self, base: &BaseRepr<T>) -> Option<BaseRepr<T>> {
        if self.checker.check(base) {
            let mut result: BaseRepr<T> = HashSet::with_capacity(base.len());
            result.extend(base.clone());
            return Some(result);
        }
        None
    }
}
