// SPDX-FileCopyrightText: 2022 BC Lib Developers
//
// SPDX-License-Identifier: Apache-2.0

use crate::BaseRepr;
use core::fmt::Debug;

pub trait Checker<T>: Debug {
    fn check(&self, input: &BaseRepr<T>) -> bool;
}

#[derive(Debug)]
pub struct NonEmptyChecker {}

impl NonEmptyChecker {
    pub fn new() -> Self {
        Self {}
    }
}

impl<T> Checker<T> for NonEmptyChecker {
    fn check(&self, input: &BaseRepr<T>) -> bool {
        !input.is_empty()
    }
}

#[derive(Debug)]
pub struct AtLeastTwoChecker {}

impl AtLeastTwoChecker {
    pub fn new() -> Self {
        Self {}
    }
}

impl<T> Checker<T> for AtLeastTwoChecker {
    fn check(&self, input: &BaseRepr<T>) -> bool {
        input.len() >= 2
    }
}
