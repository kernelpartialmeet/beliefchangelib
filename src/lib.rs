// SPDX-FileCopyrightText: 2022 BC Lib Developers
//
// SPDX-License-Identifier: Apache-2.0

//! # BC lib
//!
//! `bclib` is a library providing well-known implementations for Belief Base
//! algorithms

#![warn(missing_docs)]

use std::collections::HashSet;



pub mod checker;
pub mod formula;
pub mod hst;
pub mod mi;

type BaseRepr<T> = HashSet<T>;

#[cfg(test)]
mod tests {
    use std::{collections::HashSet, rc::Rc};

    use crate::{
        checker::{AtLeastTwoChecker, Checker},
        mi::{
            classical_mis::ClassicalMIS,
            divider_conquer_mis::DivideAndConquerMIS,
            mic::{BlackBoxMIC, MICalculator},
            trivial_mie::TrivialMIE,
        },
        BaseRepr,
    };

    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }

    #[test]
    fn basic_test() {
        let str_vec = ["hello", "rust", "world"];
        let mut string_vec = Vec::new();
        let mut base: BaseRepr<String> = HashSet::with_capacity(3);

        for some_str in str_vec {
            string_vec.push(String::from(some_str));
        }

        for x in string_vec {
            base.insert(x);
        }

        for x in &base {
            println!("{:?}", x)
        }

        let checker = AtLeastTwoChecker::new();
        let checker_rc: Rc<dyn Checker<String>> = Rc::new(checker);
        let mie = TrivialMIE::new(Rc::clone(&checker_rc));
        let mis = ClassicalMIS::new(Rc::clone(&checker_rc));
        let mis2 = DivideAndConquerMIS::new(Rc::clone(&checker_rc));
        let mic = BlackBoxMIC::new(Box::new(mie), Box::new(mis));
        let mic2 = BlackBoxMIC::new(
            Box::new(TrivialMIE::new(Rc::clone(&checker_rc))),
            Box::new(mis2),
        );

        assert!(mic.minimp(&base).is_some());
        assert!(mic2.minimp(&base).is_some());
    }
}
