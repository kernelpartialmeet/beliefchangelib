// SPDX-FileCopyrightText: 2022 BC Lib Developers
//
// SPDX-License-Identifier: Apache-2.0

use std::fmt::Debug;
use std::fmt::Display;
use std::hash::Hash;

pub trait Formula: Hash + Eq + Clone + Debug + Display {}

impl Formula for String {}
