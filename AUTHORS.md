<!--
SPDX-FileCopyrightText: 2022 BC Lib Developers

SPDX-License-Identifier: CC0-1.0
-->

The following is an incomplete list of contributors:

# Core

Guimarães, Ricardo
S. Ribeiro, Jandson

# Additional
